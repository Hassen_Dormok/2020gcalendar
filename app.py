#!/usr/bin/env python
# -*- coding: utf-8 -*-

# importing tkinter and tkinter.ttk 
# and all their functions and classes 
from __future__ import print_function
import datetime
import pickle
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google_auth_oauthlib.flow import Flow
from google.auth.transport.requests import Request
import time 
import tkinter as tk 
from tkinter import ttk
from tkinter import *
from tkinter.ttk import *
from tkinter.filedialog import askopenfilename
import os.path
import os 
import sys
import json
import pytz
import pyperclip
import datetime as dt # import the module not the type datetime
from icalendar import Calendar, Event, vText , vDatetime
from datetime import date, timedelta , datetime
from time import strftime,gmtime
from tkinter import filedialog
import tkinter as tk
from PIL import Image, ImageTk
from itertools import count
import urllib.parse
import webbrowser
from constants import optionsMonths,optionsDays,optionsHours,optionsMinutes,optionsYears,optionsSecondes, optionsFrequencies
class GifDisplayer(tk.Label):
    
    def load(self, im ):
        if isinstance(im, str):
            im = Image.open(im)
        self.loc = 0
        self.frames = []

        try:
            for i in count(1):
                self.frames.append(ImageTk.PhotoImage(im.copy()))
                im.seek(i)
        except EOFError:
            pass

        try:
            self.delay = im.info['duration']
        except:
            self.delay = 100

        if len(self.frames) == 1:
            self.config(image=self.frames[0])
        else:
            self.next_frame()

    def unload(self):
        self.config(image="")
        self.frames = None

    def next_frame(self):
        if self.frames:
            self.loc += 1
            self.loc %= len(self.frames)
            self.config(image=self.frames[self.loc])
            self.after(self.delay, self.next_frame)

def popup(msg , frame , failOrSuccess):
    popup =Tk()
    popup.geometry("260x20") 
    tk.Label(popup, text=msg , bg = "red" , fg= "white").pack()
    popup.after(2000, lambda: popup.destroy())


def alert(msg):
    root = Toplevel()
    root.title('ERROR LOADING')
    root.geometry("500x400")
    lbl = GifDisplayer(root)
    lbl.load('animations/error.gif') 
    lbl.pack()
    messageLabel = Label(root , text = msg, background="white" , foreground = "brown4" )
    messageLabel.place(relx = 0.5,  
                rely = 0.75, 
                anchor = 'center' ) 

    root.after(3500, lambda:root.destroy()) 
    
    #popup =Tk()
    #popup.geometry("230x20")
    #tk.Label(popup, text=msg , bg = "red" , fg= "white").pack()
    #popup.after(1500, lambda: popup.destroy())

def onlinkSelection(lb):
    global linkselected
    if(lb.curselection()):
        linkselected = lb.get(lb.curselection()[0])

def accessSelectedLink(linkselected):
    webbrowser.open(linkselected, new=0, autoraise=True)


def eventsInsertionWnd(frame , message , linkslist ,sourceFile , eventNumber):
    global linkselected
    runProgressSave(frame , message)
    createdEventsLinks = Toplevel()
    createdEventsLinks.title('Links')
    createdEventsLinks.geometry("500x400")
    createdEventsLinks.config(bg='bisque')

    openbrowser = PhotoImage(file="icons/openinbrowser.png")


    filename = sourceFile.rsplit('/', 1)
    #print(filename[len(filename)-1]) #only filename
    textlabel = Label(createdEventsLinks , text = str(eventNumber)+"évènements inserés depuis " + filename[len(filename)-1] + ": ", width = 65, background="bisque", relief= "solid" , borderwidth = 3 , foreground = "salmon4" , font = 'Helvetica 8 bold')
    textlabel.place(x=30,y=50)
    var1 = StringVar(value=linkslist)
    links_listbox = Listbox(createdEventsLinks, listvariable=var1,  height = 10,  
                      width =60 ,  
                      bg = "wheat2",
                      borderwidth=0,
                      highlightthickness=0,
                      activestyle = 'underline',   
                      font = "Helvetica 10 bold", 
                      selectbackground = "sandybrown",
                      fg = "tan4") 
    links_listbox.place(x = 30 , y = 70)

    links_listbox.bind("<<ListboxSelect>>", lambda x:onlinkSelection(links_listbox))

    accessURLbtn = tk.Button(createdEventsLinks ,text = 'Ouvrir dans le navigateur',bg='bisque' ,image = openbrowser, compound = 'left' , command=lambda:accessSelectedLink(linkselected))
    accessURLbtn.image = openbrowser
    accessURLbtn.place(x = 100 , y = 280) 



def runProgressSave(frame , message):

    root = Toplevel()
    root.title('CONFIRMATION')
    root.geometry("500x400")
    lbl = GifDisplayer(root)
    lbl.load('animations/check.gif') 
    lbl.pack()
    messageLabel = Label(root , text = message, background="white" , foreground = "green" ,  
                      )
    messageLabel.place(relx = 0.5,  
                rely = 0.75, 
                anchor = 'center' ) 
    root.after(2000, lambda:root.destroy()) 
    frame.destroy()

def successWithoutdestroying(message):

    root = Toplevel()
    root.title('CONFIRMATION')
    root.geometry("500x400")
    lbl = GifDisplayer(root)
    lbl.load('animations/check.gif') 
    lbl.pack()
    messageLabel = Label(root , text = message, background="white" , foreground = "green" ,  
                      )
    messageLabel.place(relx = 0.5,  
                rely = 0.75, 
                anchor = 'center' ) 
    root.after(2000, lambda:root.destroy()) 


def clipboardCopy(url):
    
    pyperclip.copy(url)
    root = Toplevel()
    root.title('CONFIRMATION COPY')
    root.geometry("200x200")
    lbl = GifDisplayer(root)
    lbl.load('animations/copied.gif') 
    lbl.pack()
    messageLabel = Label(root , text = "URL copié !", background="white" , foreground = "black" ,  
                      )
    messageLabel.place(relx = 0.5,  
                rely = 0.1, 
                anchor = 'center' ) 
    root.after(1800, lambda:root.destroy()) 
    #url.select_range(0, END)
    #tk.Label(popup, text="Votre url est copié dans votre presse papier" , bg = "green" , fg= "white").pack()
    #root.after(1000, lambda: popup.destroy())

def link_window(url , root):
    window =Toplevel()
    window.title("Link authorization")
    window.geometry("500x400")
    window.configure(bg='bisque')
    label = tk.Label(window, text = "Lien d'autorisation :" , bg = 'bisque').place(x=30,y=70)
    
    #Link textfield
    link = Entry(window)
    link.place(x=190 , y = 70)
    link.focus_set()
    link.config(width=35)
    link.insert(0,url)

    #Code Label
    label = tk.Label(window, text = "Entrer le code de validation :"  , bg='bisque').place(x=30,y=100)

    #Code textfield
    code = Entry(window)
    code.pack()
    code.place(x=190 , y = 100)
    code.focus_set()
    code.config(width=50)

    cpytoClipboard = Button(window , text="copier" , command = lambda:clipboardCopy(url))
    cpytoClipboard.place (x = 410 , y = 70)

    authBtn = tk.Button(window , text="Enregistrer le compte " , background = 'bisque',command = lambda:saveCodeClient(code , window , root))
    authBtn.place(x=190 , y = 160)

def scopesANDflow():
    SCOPES = ['https://www.googleapis.com/auth/userinfo.email',
        'openid','https://www.googleapis.com/auth/calendar',
        'https://www.googleapis.com/auth/userinfo.profile']
    flow = InstalledAppFlow.from_client_secrets_file(
            'client_secret.json', SCOPES ,redirect_uri='urn:ietf:wg:oauth:2.0:oob')
    return flow

def getURL():
    flow = scopesANDflow()
    auth_url, _ = flow.authorization_url(prompt='consent')
    return auth_url


def loadClientstoAlist():

    client_list = []
    creationdates = []
    directory = os.fsencode("savedClients")
    for file in os.listdir(directory):
         filename = os.fsdecode(file)
         client_list.append(filename.capitalize())
         creationdates.append('crée le :'+datetime.fromtimestamp(os.path.getctime('savedClients/'+filename)).strftime('%Y-%m-%d'))
         #print(datetime.fromtimestamp(os.path.getctime('savedClients/'+filename)).strftime('%Y-%m-%d %H:%M:%S'))
          
    return client_list , creationdates

def saveCodeClient(textfieldCode , frame , root):
    creds = None
    flow = scopesANDflow()
    
    try : 
        if textfieldCode.index("end") == 0:
            alert("Veuillez insérer le code d'authorization !")
        else : 
            flow.fetch_token(code = textfieldCode.get())
            creds = flow.credentials
            session = flow.authorized_session()
            profile_info = session.get(
            'https://www.googleapis.com/userinfo/v2/me').json()
            
            if os.path.exists('savedClients/'+profile_info['name']):
                print("this user already exists !! ")
                alert("Cet utilisateur existe dèja !")
                return 
            else:
                runProgressSave(frame , "Le compte a bien été enregistré !")
                
            with open('savedClients/'+profile_info['name'], 'wb') as token:
                pickle.dump(creds, token)

            os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
    except : 
        print("wrong code")
        alert("  Authorization refusé ! \nLe code entré est erroné ")
    #save users info in file after authorization
    #file = open("savedClients/"+profile_info['name'] + "_" +profile_info['id'], "w") 
    #file.write("identifiant : " + profile_info['id'] +"\nnom et prenom : " + profile_info['name'] + "\nlocalisation : " + profile_info['locale']) 
    #file.close() 

def getCalendarsNamesList(service):
    lc = [] #summary list
    lcid = [] #ids list
    ltimeZones = []

    calendar_list_entry = service.calendarList().list().execute()
    for calendar in calendar_list_entry['items'] : 
        #print (calendar['summary'])
        lc.append(calendar['summary'])
        lcid.append(calendar['id'])
        ltimeZones.append(calendar['timeZone'])

    #print(*lc ,  sep = "\n")
    return lc , lcid , ltimeZones

def createNewCalendar(frame ,service , summary , timeZone):
    #print(summary)
    calendar = {
    'summary': summary,
    'timeZone': timeZone
    }
    print("TIMEZONE" , timeZone , "SUMMARY" , summary)
    newcalendar = service.calendars().insert(body=calendar).execute()
    runProgressSave(frame , "Le nouveau calendrier a bien été crée !")
    frame.destroy()


    return newcalendar


def newcalendarWindow(service):
    filepath="timeZoneList.txt"
    imgValidate = PhotoImage(file="icons/create.png")
    options = []
    newCalwindow =Toplevel()
    newCalwindow.title("Nouveau calendrier")
    newCalwindow.geometry("500x250")
    newCalwindow.configure(bg='bisque')

    calendarName = Label( newCalwindow , text = "Entrer le nom du nouveau calendrier ")
    calendarName.place(x = 50 , y = 50 )

    nametyped = Entry(newCalwindow)
    nametyped.place(x = 300 , y = 50)

    with open(filepath) as file_in:
        for line in file_in:
            options.append(line)

    variable = StringVar(value = options)
    #variable.set(options[0]) 

    timeZone = Label( newCalwindow , text = "Choisissez votre fuseau horaire")
    timeZone.place(x = 50 , y = 100)

    w = OptionMenu(*(newCalwindow, variable) + tuple(options))
    w.place(x = 300 , y = 100)

    createCalBtn = Button(newCalwindow , image = imgValidate , compound = "left" ,text = "Créer" , width = 15 , command = lambda:createNewCalendar(newCalwindow, service , nametyped.get() , variable.get()[3:-3]))
    createCalBtn.photo = imgValidate
    createCalBtn.place(x = 300 , y = 190)

def getSelectedIdCalendar(lb , ids , listNames,service):
    global calid 
    global nameselectedCal
    global currentTcz


    if(lb.curselection()):
        selection = lb.curselection()[0]
        #print(selection)
        calid = ids[selection]
        nameselectedCal = listNames[selection]
        saveLocationAndParticipantsFiles(service , calid , nameselectedCal)

        currcalendar = service.calendars().get(calendarId=calid).execute()
        currentTcz = currcalendar['timeZone']


def deleteCalendar(service , calId , frame, lb):
    #try : 
    if(lb.curselection()):
        service.calendars().delete(calendarId=calId).execute()
        runProgressSave(frame , "Le calendrier selectionné a bien été supprimé")
    else : 
        alert("Veuillez selectionner un calendrier !")
    #except : 
    #    alert("Veuillez séléctionner un calendrier à supprimer !")



def addNewEvent(frame , service , calid , summary , location , description , startDate , endDate , timeZone , recurrence , attendees , reminders ):
    event = {
      'summary': summary,
      'location': location,
      'description': description,
      'start': {
        'dateTime': startDate,
        'timeZone': timeZone,
      },
      'end': {
        'dateTime': endDate,
        'timeZone': timeZone,
      },
      'recurrence': []
        #'RRULE:FREQ=DAILY;COUNT=2'
      ,
      'attendees': attendees , 
      #[
      ##  {'email': 'lpage@example.com'},
      #  {'email': 'sbrin@example.com'},
      #],
      'reminders': {
        'useDefault': False,
        'overrides': [
          {'method': 'email', 'minutes': 24 * 60},
          {'method': 'popup', 'minutes': 10},
        ],
      },
    }

    try : # the only necessary two fields to have the event created on google are the two dates and TimeZOne !!
      
        if(len(summary)==0):
            alert(" Echec création de l'évènement !\nVeuillez entrer un nom à l'évènement")
        #location is always filled by default optionsMenu!!
        #we already put in description an empty string in addEventWn() whenever it's undefined so we can test 
        elif(len(description)==0):
            alert(" Echec création de l'évènement !\nVeuillez choisir les participants à l'évènement")
        elif (timeZone not in pytz.all_timezones):
            alert(" Echec création de l'évènement !\nFuseau horaire non validé !\nVeuillez entrer un fuseau horaire valide" )
        

        else :
            print("event" , event)
            if(checkTimeSlotsDisponibility(service , calid , event ,frame)):
                event = service.events().insert(calendarId=calid, body=event).execute()
                print(event)
                #print ('Event created: %s' % (event.get('htmlLink')))
                runProgressSave(frame , "L'évènement a bien été crée sur votre compte Google Calendar !")
                createdEventLinkWnd(event.get('htmlLink'))
            
            
    except : 

        if ((not isinstance(startDate, dt.datetime)) and (not isinstance(endDate, dt.datetime)) and (len(timeZone)==0)):
            alert(" Echec création de l'évènement !\nVeuillez remplir tous les champs ")

        #elif ((len(startDate)==0)&& (len(timeZone)==0)):
        #    alert(" Echec création de l'évènement !\nVeuillez remplir tous les champs ")
        #elif ((len(startDate)==0)&& (len(endDate)==0)):
        #    alert(" Echec création de l'évènement !\nVeuillez remplir tous les champs ")
        #elif ((len(endDate)==0)&& (len(timeZone)==0)):
        #    alert(" Echec création de l'évènement !\nVeuillez remplir tous les champs ")
        #elif (len(startDate)==0):
        #    alert(" Echec création de l'évènement !\nVeuillez entrer une date de début à l'évènement")
        elif (not location):
            alert(" Echec création de l'évènement !\nVeuillez entrer une localisation à l'évènement")
        elif (not endDate):
            alert(" Echec création de l'évènement !\nVeuillez entrer une date de fin à l'évènement")


def displayAttendees(attendees):
    s=''
    if(len(attendees) == 1):
        c=" {'email' :"
        s +=  c + '"' + str(attendees[0]) + '"' + '}'

    else:
        for a in attendees :
               
            c=" {'email' :"
            s +=  c + '"' + str(a) + '"' + '},\n'

    return s

def addEventWithPreferences(frame , service , calid , summary , location , description , startDate , endDate , timeZone , freq, intervalle , count , bday , attendees , reminders ):
    
    #attendees = attendees.split(',')
    print('attendees',attendees)
    if(bday):    #optionnal preference day
        event = {
          'summary': summary,
          'location': location,
          'description': description,
          'start': {
            'dateTime': startDate,
            'timeZone': timeZone,
          },
          'end': {
            'dateTime': endDate,
            'timeZone': timeZone,
          },
          'recurrence': ['RRULE:FREQ='+freq+';COUNT='+count+';INTERVAL='+intervalle+';BYDAY='+bday]
          ,
          'attendees':  
          [  {'email': attendees},
          ##  {'email': 'lpage@example.com'},
          #  {'email': 'sbrin@example.com'},
          ],
          'reminders': {
            'useDefault': False,
            'overrides': [
              {'method': 'email', 'minutes': 24 * 60},
              {'method': 'popup', 'minutes': 10},
            ],
          },
        }

    else:
        event = {
          'summary': summary,
          'location': location,
          'description': description,
          'start': {
            'dateTime': startDate,
            'timeZone': timeZone,
          },
          'end': {
            'dateTime': endDate,
            'timeZone': timeZone,
          },
          'recurrence': ['RRULE:FREQ='+freq+';COUNT='+count+';INTERVAL='+intervalle]
          ,
          'attendees': 
          [ 
          #[
            {'email': attendees},
          #  {'email': 'sbrin@example.com'},
          ],
          'reminders': {
            'useDefault': False,
            'overrides': [
              {'method': 'email', 'minutes': 24 * 60},
              {'method': 'popup', 'minutes': 10},
            ],
          },
        }
    try : 
        print(event['attendees'])
        event = service.events().insert(calendarId=calid, body=event).execute()
        #print(event['attendees'].get('email', event['attendees'].get('email')))
                #print ('Event created: %s' % (event.get('htmlLink')))
        runProgressSave(frame , "L'évènement a bien été crée sur votre compte Google Calendar !")
        createdEventLinkWnd(event.get('htmlLink'))
    except : 
        alert("Impossible de programmer l'évènement ! \nVeuillez bien remplir les champs")


def catchselection(event):
    global variablelieu
    if (isinstance(variablelieu, tk.StringVar)):
        variablelieu = variablelieu.get()[:-1] #removing line break 
    pass

def catchselectionPref(event):
    global variablelieuPref
    if (isinstance(variablelieuPref, tk.StringVar)):
        variablelieuPref = variablelieuPref.get()[:-1] #removing line break 
    pass

def catch_start_Day(event):    
    global variableDay
    if (isinstance(variableDay, tk.StringVar)):
       variableDay = int(variableDay.get())
    pass
    
def catch_start_Month(event):
    global variableMonth
    if(isinstance(variableMonth, tk.StringVar)):
        variableMonth = int(variableMonth.get())
    pass
def catch_start_Year(event):
    global variableYear
    if(isinstance(variableYear, tk.StringVar)):
        variableYear = int(variableYear.get())
    pass
def catch_start_hour(event):
    global variableHour
    if (isinstance(variableHour, tk.StringVar)):
       variableHour = int(variableHour.get())
       pass
        
def catch_start_min(event):
    global variableMinutes
    if(isinstance(variableMinutes, tk.StringVar)):
        variableMinutes = int(variableMinutes.get())
        pass
def catch_start_sec(event):
    global variableSec 
    if(isinstance(variableSec, tk.StringVar)):
        variableSec = int(variableSec.get())
        pass

def catch_end_Day(event):    
    global variableDay2
    if (isinstance(variableDay2, tk.StringVar)):
       variableDay2 = int(variableDay2.get())
       pass
        
def catch_end_Month(event):
    global variableMonth2
    if(isinstance(variableMonth2, tk.StringVar)):
        variableMonth2 = int(variableMonth2.get())
        pass

def catch_end_Year(event):
    global variableYear2
    if(isinstance(variableYear2, tk.StringVar)):
        variableYear2 = int(variableYear2.get())
        pass
def catch_end_hour(event):
    global variableHour2
    if (isinstance(variableHour2, tk.StringVar)):
       variableHour2 = int(variableHour2.get())
       pass
        
def catch_end_min(event):
    global variableMinutes2
    if(isinstance(variableMinutes2, tk.StringVar)):
        variableMinutes2 =int(variableMinutes2.get())
        pass
def catch_end_sec(event):
    global variableSec2 
    if(isinstance(variableSec2, tk.StringVar)):
        variableSec2 = int(variableSec2.get())
        pass

def catch_frequency(event):
    global variablefreq 
    if(isinstance(variablefreq, tk.StringVar)):
        variablefreq = variablefreq.get()
        pass


def programEventWnd(service , calid): 
    programEventsWnd = Toplevel()
    programEventsWnd.geometry("500x400")
    programEventsWnd.title('Programmer un évènement')
    programEventsWnd.configure(bg='bisque')

    global variableDay, variableMonth,variableYear, variableHour,variableMinutes,variableSec
    global variableHour2,variableMinutes2,variableSec2
    global variablefreq
    global variablelieuPref
    global descEntry

    optionslieu =['--']
    pencil = PhotoImage(file = "icons/addevPencil.png")
    imgValid = PhotoImage(file="icons/create.png")

    summarylbl = Label(programEventsWnd ,  image = pencil , text = "Nom de l'évènement :" ,compound = "left" , background = "bisque" , foreground = "OrangeRed3")
    summarylbl.photo = pencil
    summarylbl.place(x = 50, y = 10)
    summaryEntry = Entry(programEventsWnd)
    summaryEntry.place(x = 300, y = 10)

    startDatelbl = Label(programEventsWnd ,  image = pencil , text = "Date et horaires du début :" ,compound = "left" , background = "bisque" , foreground = "OrangeRed3")
    startDatelbl.photo = pencil
    startDatelbl.place(x = 50, y = 35)

    #indicators
    dateIndLbl = Label(programEventsWnd , text ='jour-mois-année' ,  background = "bisque" , foreground = "OrangeRed3" , font ='Helvetica 7')
    dateIndLbl.place(x = 220, y = 35)

    hourstartIndLbl = Label(programEventsWnd , text ='horaire du début' ,  background = "bisque" , foreground = "OrangeRed3" , font ='Helvetica 7')
    hourstartIndLbl.place(x = 220, y = 55)

    hourendIndLbl = Label(programEventsWnd , text ='horaire de la fin' ,  background = "bisque" , foreground = "OrangeRed3" , font ='Helvetica 7')
    hourendIndLbl.place(x = 220, y = 75)

    #champs
    with open("ressources/" +nameselectedCal + "/lieux.txt") as file_in:
        for line in file_in:
            optionslieu.append(line)

    variablelieuPref = tk.StringVar(value = optionslieu)
    wl = OptionMenu(programEventsWnd , variablelieuPref ,*(optionslieu) , command =catchselectionPref)
    wl.place(x = 300, y = 140)

    variableDay = tk.StringVar(value = optionsDays)
    wDay = OptionMenu(programEventsWnd , variableDay , *(optionsDays) , command =catch_start_Day)
    wDay.place(x = 300, y = 35)

    variableMonth = tk.StringVar(value = optionsMonths)
    wMth = OptionMenu(programEventsWnd , variableMonth , *(optionsMonths) , command =catch_start_Month)
    wMth.place(x = 350, y = 35)

    variableYear = tk.StringVar(value = optionsYears)
    wYr = OptionMenu(programEventsWnd , variableYear , *(optionsYears) , command =catch_start_Year)
    wYr.place(x = 420, y = 35)

    variableHour = tk.StringVar(value = optionsHours)
    wHr = OptionMenu(programEventsWnd , variableHour , *(optionsHours) , command =catch_start_hour)
    wHr.place(x = 300, y = 55)

    variableMinutes = tk.StringVar(value = optionsMinutes)
    wMin = OptionMenu(programEventsWnd , variableMinutes , *(optionsMinutes) , command =catch_start_min)
    wMin.place(x = 360, y = 55)

    variableSec = tk.StringVar(value = optionsMinutes)
    wSec = OptionMenu(programEventsWnd , variableSec , *(optionsMinutes) , command =catch_start_sec)
    wSec.place(x = 420, y = 55)

    variableHour2 = tk.StringVar(value = optionsHours)
    wHr2 = OptionMenu(programEventsWnd , variableHour2 , *(optionsHours) , command =catch_end_hour)
    wHr2.place(x = 300, y = 75)

    variableMinutes2 = tk.StringVar(value = optionsMinutes)
    wMin2 = OptionMenu(programEventsWnd , variableMinutes2 , *(optionsMinutes) , command =catch_end_min)
    wMin2.place(x = 360, y = 75)

    variableSec2 = tk.StringVar(value = optionsMinutes)
    wSec2 = OptionMenu(programEventsWnd , variableSec2 , *(optionsMinutes) , command =catch_end_sec)
    wSec2.place(x = 420, y = 75)

    timeZonelbl = Label(programEventsWnd ,  image = pencil , text = "Fuseau horaire :" ,compound = "left" , background = "bisque" , foreground = "OrangeRed3")
    timeZonelbl.photo = pencil
    timeZonelbl.place(x = 50, y = 107)
    timeZoneEntry = Entry(programEventsWnd)
    timeZoneEntry.place(x = 300, y = 107)



    #optionslieu = list(dict.fromkeys(optionslieu))

    locationlbl = Label(programEventsWnd , image = pencil , text = "Localisation de l'évènement " ,compound = "left", background = "bisque" , foreground = "OrangeRed3")
    locationlbl.photo = pencil
    locationlbl.place(x = 50, y = 140)


    desclbl = Label(programEventsWnd , image = pencil , text = "Déscription de l'évènement " ,compound  = "left" ,  background = "bisque" , foreground = "OrangeRed3")
    desclbl.photo = pencil
    desclbl.place(x = 50, y = 180)
    goTochoicesBtn =tk.Button(programEventsWnd , text ="choisir les participants" , command = lambda:multipleChoicesListboxParticipantsFrame(goTochoicesBtn) , width = 17)
    goTochoicesBtn.place(x = 300, y = 180)

    freqlbl = Label(programEventsWnd ,  image = pencil , text = "Fréquence de l'évènement :" ,compound = "left" , background = "bisque" , foreground = "OrangeRed3")
    freqlbl.photo = pencil
    freqlbl.place(x = 50, y = 210)
    variablefreq = tk.StringVar(value = optionsFrequencies)
    wl = OptionMenu(programEventsWnd , variablefreq ,*(optionsFrequencies) , command =catch_frequency)
    wl.place(x = 300, y = 210)

    intervallelbl = Label(programEventsWnd ,  image = pencil , text = "Intervalle entre deux répétitions :" ,compound = "left" , background = "bisque" , foreground = "OrangeRed3")
    intervallelbl.photo = pencil
    intervallelbl.place(x = 50, y = 240)
    intervalleEntry = Entry(programEventsWnd)
    intervalleEntry.place(x = 300, y = 240)

    occurencesNumberlbl = Label(programEventsWnd ,  image = pencil , text = "Nombre de répétitions :" ,compound = "left" , background = "bisque" , foreground = "OrangeRed3")
    occurencesNumberlbl.photo = pencil
    occurencesNumberlbl.place(x = 50, y = 270)
    occurencesNumberEntry = Entry(programEventsWnd)
    occurencesNumberEntry.place(x = 300, y = 270)

    preferenceDaylbl = Label(programEventsWnd ,  image = pencil , text = "Préférence jour :" ,compound = "left" , background = "bisque" , foreground = "OrangeRed3")
    preferenceDaylbl.photo = pencil
    preferenceDaylbl.place(x = 50, y = 300)
    preferenceDayEntry = Entry(programEventsWnd)
    preferenceDayEntry.place(x = 300, y = 300)

    invitelbl = Label(programEventsWnd ,  image = pencil , text = "Invités:" ,compound = "left" , background = "bisque" , foreground = "OrangeRed3")
    invitelbl.photo = pencil
    invitelbl.place(x = 50, y = 330)
    inviteEntry = Entry(programEventsWnd)
    inviteEntry.place(x = 300, y = 330)

    try :
        if (descEntry is None):
                descEntry = '' #comme ca on peut controler le field des participants plutard
    except:
        descEntry = '' #forced 
    

    createbtn = Button(programEventsWnd , image = imgValid,  text = "Créer" , compound = "left" , width = 15, 
        command=lambda:addEventWithPreferences(programEventsWnd , service , calid , summaryEntry.get() , variablelieuPref , descEntry , 
            datetime(variableYear, variableMonth, variableDay, variableHour, variableMinutes, variableSec,
        tzinfo=pytz.timezone(timeZoneEntry.get())).strftime("%Y-%m-%dT%H:%M:%S") , datetime(variableYear, variableMonth, variableDay, variableHour2, variableMinutes2, variableSec2,
        tzinfo=pytz.timezone(timeZoneEntry.get())).strftime("%Y-%m-%dT%H:%M:%S") ,timeZoneEntry.get(), variablefreq, intervalleEntry.get() , occurencesNumberEntry.get() , preferenceDayEntry.get() , inviteEntry.get() , ''))
    createbtn.photo = imgValid
    createbtn.place(x = 300 , y = 360)




def eventsMenu(service , calid ,lb):
    if(lb.curselection()):
        menuEvents = Toplevel()
        menuEvents.geometry("500x400")
        menuEvents.title('Gestion des évènements')
        menuEvents.configure(bg='bisque')

        addevent = PhotoImage(file="icons/addevent.png")
        delevent = PhotoImage(file="icons/deleteevent.png")
        programevent = PhotoImage(file="icons/automatic.png")

        addEventsMenu = Label(menuEvents, image = addevent ,text = " Ajouter un évènement" , foreground = 'LightSalmon3',  compound = "top" , background = 'bisque' , font = "Helvetica 8 italic" )
        #addc.config(image = imgAddcalendar)
        addEventsMenu.photo = addevent
        addEventsMenu.place(x=120 , y = 110)

        addEventsMenu.bind("<Button-1>",lambda x:addEventWnd(service , calid))

        delEventsMenu = Label(menuEvents, image = delevent ,text = " Supprimer un évènement" , foreground = 'LightSalmon3',  compound = "top" , background = 'bisque' , font = "Helvetica 8 italic")
        #addc.config(image = imgAddcalendar)
        delEventsMenu.photo = delevent
        delEventsMenu.place(x=250 , y = 110)

        delEventsMenu.bind("<Button-1>",lambda x:deleteEventWnd(service , calid))

        programsMenu = Label(menuEvents, image = programevent ,text = "Programmer un évènement" , foreground = 'LightSalmon3',  compound = "top" , background = 'bisque' , font = "Helvetica 8 italic")
        #addc.config(image = imgAddcalendar)
        programsMenu.photo = programevent
        programsMenu.place(x=180 , y = 220)

        programsMenu.bind("<Button-1>",lambda x:programEventWnd(service , calid))
    else : 
        alert("Veuillez selectionner un calendrier ! ")


def destroyAndreplaceButton(wnd , btn , returnedList):
    global descEntry
    wnd.destroy()
    btn.config(text = '\u2713' , fg='green')

    returnedList = [x[:-1] for x in returnedList]
    

    descEntry = '\n'.join(map(str, returnedList))
    

def multipleChoicesListboxParticipantsFrame(tobeChangedBtn):
    global nameselectedCal
    global returnedList
    #listselection = []
    returnedList = []
    participants_list = []
    toplevelFramewnd= Toplevel()
    frameMultipleChoices = tk.Frame(toplevelFramewnd)
    frameMultipleChoices.configure(bg='bisque') 
    frameMultipleChoices.pack(side = RIGHT)

    with open("ressources/" +nameselectedCal + "/participants.txt") as file_in:
        for line in file_in:
            participants_list.append(line)

    #participants_list = list(dict.fromkeys(participants_list))

    listVar = StringVar(value = participants_list)

    scrollbar = Scrollbar(frameMultipleChoices) 
    scrollbar.pack(side=RIGHT, fill=Y)

    
    participants_box = Listbox(frameMultipleChoices, listvariable = listVar , selectmode = "multiple" ,  yscrollcommand=scrollbar.set)
    
    participants_box.pack()

    scrollbar.config(command=participants_box.yview)

    participants_box.bind("<<ListboxSelect>>", lambda x:onParticipantsSelection(participants_box))


    ok_btn = Button(frameMultipleChoices, text = " OK !" ,command = lambda:destroyAndreplaceButton(toplevelFramewnd , tobeChangedBtn , returnedList))
    ok_btn.pack(side = BOTTOM , fill =X)
    


def onParticipantsSelection(lb):
    global returnedList
    selectedNewList =[]
    if(lb.curselection()):
        for element in lb.curselection():
            selectedNewList.append(lb.get(element))
    returnedList = selectedNewList


    #print(variableDay2,' ',variableMonth2,' ',variableYear2 , ' ',variableHour2 , ' ', variableMinutes2,' ', variableSec2)

def addEventWnd(service, calid):
    global nameselectedCal
    global returnedList
    global descEntry
    global variablelieu 
    global variableDay, variableMonth,variableYear, variableHour,variableMinutes,variableSec
    global variableDay2, variableMonth2,variableYear2,variableHour2,variableMinutes2,variableSec2

    editcalendar = Toplevel()
    editcalendar.geometry('500x400')
    editcalendar.title('Gestion des évènements')
    editcalendar.configure(bg='bisque')
    optionslieu = ['--']

    returnedSelection = []
    imgValid = PhotoImage(file="icons/create.png")
    pencil = PhotoImage(file = "icons/addevPencil.png")

    summarylbl = Label(editcalendar ,  image = pencil , text = "Nom de l'évènement " ,compound = "left" , background = "bisque" , foreground = "OrangeRed3")
    summarylbl.photo = pencil
    summarylbl.place(x = 50, y = 20)
    summaryEntry = Entry(editcalendar)
    summaryEntry.place(x = 300, y = 20)

    with open("ressources/" +nameselectedCal + "/lieux.txt") as file_in:
        for line in file_in:
            optionslieu.append(line)

    #optionslieu = list(dict.fromkeys(optionslieu))

    locationlbl = Label(editcalendar , image = pencil , text = "Localisation de l'évènement " ,compound = "left", background = "bisque" , foreground = "OrangeRed3")
    locationlbl.photo = pencil
    locationlbl.place(x = 50, y = 70)

    variablelieu = tk.StringVar(value = optionslieu)
    wl = OptionMenu(editcalendar , variablelieu ,*(optionslieu) , command =catchselection)
    wl.place(x = 300, y = 70)


    #selectedLocation = variablelieu.get()[:-1] # removing the line break caracter but let it in option Menu list 
    #print("selected location" , selectedLocation)

    desclbl = Label(editcalendar , image = pencil , text = "Déscription de l'évènement " ,compound  = "left" ,  background = "bisque" , foreground = "OrangeRed3")
    desclbl.photo = pencil
    desclbl.place(x = 50, y = 120)
    goTochoicesBtn =tk.Button(editcalendar , text ="choisir les participants" , command = lambda:multipleChoicesListboxParticipantsFrame(goTochoicesBtn) , width = 17)
    goTochoicesBtn.place(x = 300, y = 120)

    #concat selected items to get description field
    

    startlbl = Label(editcalendar ,image = pencil ,  text = "Début de l'évènement " ,compound = "left" ,  background = "bisque" , foreground = "OrangeRed3")
    startlbl.photo = pencil
    startlbl.place(x = 50, y = 170)

    dateIndLbl = Label(editcalendar , text ='Date' ,  background = "bisque" , foreground = "OrangeRed3" , font ='Helvetica 7')
    dateIndLbl.place(x = 250, y = 170)

    HoraireIndLbl = Label(editcalendar , text ='Horaire' ,  background = "bisque" , foreground = "OrangeRed3" , font ='Helvetica 7')
    HoraireIndLbl.place(x = 250, y = 190)

    variableDay = tk.StringVar(value = optionsDays)
    wDay = OptionMenu(editcalendar , variableDay , *(optionsDays) , command =catch_start_Day)
    wDay.place(x = 300, y = 170)

    variableMonth = tk.StringVar(value = optionsMonths)
    wMth = OptionMenu(editcalendar , variableMonth , *(optionsMonths) , command =catch_start_Month)
    wMth.place(x = 350, y = 170)

    variableYear = tk.StringVar(value = optionsYears)
    wYr = OptionMenu(editcalendar , variableYear , *(optionsYears) , command =catch_start_Year)
    wYr.place(x = 420, y = 170)

    variableHour = tk.StringVar(value = optionsHours)
    wHr = OptionMenu(editcalendar , variableHour , *(optionsHours) , command =catch_start_hour)
    wHr.place(x = 300, y = 190)

    variableMinutes = tk.StringVar(value = optionsMinutes)
    wMin = OptionMenu(editcalendar , variableMinutes , *(optionsMinutes) , command =catch_start_min)
    wMin.place(x = 360, y = 190)

    variableSec = tk.StringVar(value = optionsMinutes)
    wSec = OptionMenu(editcalendar , variableSec , *(optionsMinutes) , command =catch_start_sec)
    wSec.place(x = 420, y = 190)

    dateIndLbl = Label(editcalendar , text ='Date' ,  background = "bisque" , foreground = "OrangeRed3" , font ='Helvetica 7')
    dateIndLbl.place(x = 250, y = 230)

    HoraireIndLbl = Label(editcalendar , text ='Horaire' ,  background = "bisque" , foreground = "OrangeRed3" , font ='Helvetica 7')
    HoraireIndLbl.place(x = 250, y = 250)

    variableDay2 = StringVar(value = optionsDays)
    wDay2 = OptionMenu(editcalendar , variableDay2 , *(optionsDays) , command =catch_end_Day)
    wDay2.place(x = 300, y = 230)

    variableMonth2 = StringVar(value = optionsMonths)
    wMth2 = OptionMenu(editcalendar , variableMonth2 , *(optionsMonths) , command =catch_end_Month)
    wMth2.place(x = 350, y = 230)

    variableYear2 = StringVar(value = optionsYears)
    wYr2 = OptionMenu(editcalendar , variableYear2 , *(optionsYears) , command =catch_end_Year)
    wYr2.place(x = 420, y = 230)

    variableHour2 = StringVar(value = optionsHours)
    wHr2 = OptionMenu(editcalendar , variableHour2 , *(optionsHours) , command =catch_end_hour)
    wHr2.place(x = 300, y = 250)

    variableMinutes2 = StringVar(value = optionsMinutes)
    wMin2 = OptionMenu(editcalendar , variableMinutes2 , *(optionsMinutes) , command =catch_end_min)
    wMin2.place(x = 360, y = 250)

    variableSec2 = StringVar(value = optionsMinutes)
    wSec2 = OptionMenu(editcalendar , variableSec2 , *(optionsSecondes) , command =catch_end_sec)
    wSec2.place(x = 420, y = 250)

    endlbl = Label(editcalendar , image = pencil, text = "Fin de l'évènement " ,compound = "left" , background = "bisque" , foreground = "OrangeRed3")
    endlbl.photo = pencil
    endlbl.place(x = 50, y = 230)
    #notezBienlbl2.place(x = 300 , y = 250)

    timeZonelbl = Label(editcalendar ,image = pencil ,  text = "fuseau horaire de l'évènement " ,compound = "left", background = "bisque" , foreground = "OrangeRed3")
    timeZonelbl.photo= pencil
    timeZonelbl.place(x = 50, y = 300)
    timeZoneEntry = Entry(editcalendar)
    timeZoneEntry.place(x = 300, y = 300)


    try :
        if (descEntry is None):
                descEntry = '' #comme ca on peut controler le field des participants plutard
    except:
        descEntry = '' #forced 
    

    createbtn = Button(editcalendar , image = imgValid,  text = "Créer" , compound = "left" , width = 15, 
        command=lambda:addNewEvent(editcalendar ,service , calid , summaryEntry.get() , variablelieu , descEntry , datetime(variableYear, variableMonth, variableDay, variableHour, variableMinutes, variableSec,
        tzinfo=pytz.timezone(timeZoneEntry.get())).strftime("%Y-%m-%dT%H:%M:%S") , datetime(variableYear2, variableMonth2,variableDay2, variableHour2, variableMinutes2, variableSec2,
        tzinfo=pytz.timezone(timeZoneEntry.get())).strftime("%Y-%m-%dT%H:%M:%S") , timeZoneEntry.get() , '' , '', ''))

    createbtn.photo = imgValid
    createbtn.place(x = 300 , y = 350)

def createdEventLinkWnd(link):
    linkWnd = Toplevel()
    linkWnd.geometry('500x400')
    linkWnd.title('lien du nouvel évènement')
    linkWnd.configure(bg="white")

    linkEntry = tk.Entry(linkWnd , bg ='gray' , fg = "white" , width = 60)
    linkEntry.insert(0, link)
    linkEntry.place(x = 30 , y = 150)

    #lbl = GifDisplayer(linkWnd)
    #lbl.load('animations/click.gif') 
    #lbl.place(x = 70 , y = 200)

    accesLinkBtn = tk.Button(linkWnd , text = "accéder à l\'évènement" , bg = "white" , fg = "green" ,  command = lambda: webbrowser.open(linkEntry.get(), new=0, autoraise=True)) 
    accesLinkBtn.place(x=150 , y = 200)



def checkTimeSlotsDisponibility(service , calid , event ,frameToDestroy):
    global returnedList
    isAvailable = False 
    eventsList =[]
    criticalslotsEvents =[]
    listparticipants = []
    finalparticipantsList = []
    listLocations = []
   
    #reconstruct event list by id list

    eventsObject = getEventObjectList(service , calid)
    
    #start = event['start'].get('dateTime')
    #end = event['end'].get('dateTime')


    tcz = event['start'].get('timeZone')
    #print('timezone' , tcz)

    fixedyearst = dt.datetime.fromisoformat(event['start'].get('dateTime')).year
    fixedmonthst = dt.datetime.fromisoformat(event['start'].get('dateTime')).month
    fixeddayst = dt.datetime.fromisoformat(event['start'].get('dateTime')).day
    fixedhourst = dt.datetime.fromisoformat(event['start'].get('dateTime')).hour
    fixedminutest = dt.datetime.fromisoformat(event['start'].get('dateTime')).minute
    fixedsecondst = dt.datetime.fromisoformat(event['start'].get('dateTime')).second

    fixedyearend = dt.datetime.fromisoformat(event['end'].get('dateTime')).year
    fixedmonthend = dt.datetime.fromisoformat(event['end'].get('dateTime')).month
    fixeddayend = dt.datetime.fromisoformat(event['end'].get('dateTime')).day
    fixedhourend = dt.datetime.fromisoformat(event['end'].get('dateTime')).hour
    fixedminuteend = dt.datetime.fromisoformat(event['end'].get('dateTime')).minute
    fixedsecondend = dt.datetime.fromisoformat(event['end'].get('dateTime')).second

    fixedstartdateformatted = datetime(fixedyearst, fixedmonthst,fixeddayst, fixedhourst, fixedminutest, fixedsecondst,
                                     tzinfo=pytz.timezone(tcz))

    fixedenddateformatted = datetime(fixedyearend, fixedmonthend,fixeddayend, fixedhourend, fixedminuteend, fixedsecondend,
                                     tzinfo=pytz.timezone(tcz))

    print( "fixed : start ", fixedstartdateformatted , ' end ', fixedenddateformatted )

    for e in eventsObject :

        currentsdate = e['start'].get('dateTime') 
        currentedate = e['end'].get('dateTime')

        yearst = dt.datetime.fromisoformat(currentsdate).year
        monthst = dt.datetime.fromisoformat(currentsdate).month
        dayst = dt.datetime.fromisoformat(currentsdate).day
        hourst = dt.datetime.fromisoformat(currentsdate).hour
        minutest = dt.datetime.fromisoformat(currentsdate).minute
        secondst = dt.datetime.fromisoformat(currentsdate).second

        yearend = dt.datetime.fromisoformat(currentedate).year
        monthend = dt.datetime.fromisoformat(currentedate).month
        dayend = dt.datetime.fromisoformat(currentedate).day
        hourend = dt.datetime.fromisoformat(currentedate).hour
        minuteend = dt.datetime.fromisoformat(currentedate).minute
        secondend = dt.datetime.fromisoformat(currentedate).second

        startdateformatted = datetime(yearst, monthst,dayst, hourst, minutest, secondst,
                                     tzinfo=pytz.timezone(tcz))

        enddateformatted = datetime(yearend, monthend,dayend, hourend, minuteend, secondend,
                                     tzinfo=pytz.timezone(tcz))

        #print(startdateformatted)

        #print("current " , e["summary"] , 'start  ',currentsdate,'end   ',currentedate)
        

        #if(e['start'].get('timeZone') is not tcz):
        #    print('before localize' , currentsdate)
        #    currentsdateconv = convert_datetime_timezone(currentsdate , e['start'].get('timeZone') , tcz)
            #currentsdateconv = currentsdate.astimezone('America/Los_Angeles')
        #    print('after localize'  , currentsdateconv)
            #currentedate = tcz.localize(currentedate)

        c1 = (startdateformatted<fixedstartdateformatted) and (enddateformatted<fixedenddateformatted) and (enddateformatted>fixedstartdateformatted) # |...[...| ]
        c2 = (startdateformatted<fixedenddateformatted) and (enddateformatted > fixedenddateformatted) and (startdateformatted>fixedstartdateformatted) 
        c3 = (startdateformatted<fixedstartdateformatted) and (enddateformatted>fixedenddateformatted) 
        
        #print(startdateformatted<fixedstartdateformatted,' and', enddateformatted,'<',end and (enddateformatted>fixedstartdateformatted,"    ",c1)
        #print(startdateformatted ," < " ,end ,"and", enddateformatted," > ",end ,"    ",c2)
        #print(startdateformatted ," < " ,start ,"and", enddateformatted," > ",end ,"    ",c3

        #pas d intersection avec le creneau fixe de l evenemnt a ajouter
        #only append critical events that are around the fixed date range
        if(c1 or c2 or c3):
            #print('im here')
        #if not (((e['start'].get('dateTime')<start) and (e['end'].get('dateTime')<start)) or (e['start'].get('dateTime') > end)) :        
            criticalslotsEvents.append(e)

    #print("longueur critical" ,len(criticalslotsEvents))
    #print("criticals" ,criticalslotsEvents)

    #checking participants disponibility
    if (len(criticalslotsEvents)==0):
        #print("always good")
        isAvailable = True
    else : 
        for criticalslot in criticalslotsEvents :
            if(len(criticalslot['description'])>1):
                listparticipants.append(criticalslot['description'].split('\n'))
                
            listLocations.append(criticalslot['location'])

        for sublist in listparticipants :
            for participant in sublist :
                finalparticipantsList.append(participant)

        #print("ppt :"  , listparticipants)
        print("final :", finalparticipantsList)
        print("lieux:" , listLocations)

        #print('final ppt list : ' ,finalparticipantsList)
        
        #print('location stuff' ,event['location']  not in listLocations)

        returnedList = [x[:-1] for x in returnedList]

        #print('returned list : ', returnedList)

        if ((common_element(finalparticipantsList , returnedList)==False) and (event['location']  not in listLocations)) :
            
            isAvailable = True

        else :
            isAvailable = False

            if (common_element(finalparticipantsList , returnedList)==True):
                print("croisement participants")
                alert("Créneau indisponible certains participants sont occupés !")
                frameToDestroy.destroy()

            elif (not event['location']  not in listLocations):
                print("croisement salle")
                alert("Créneau indisponible , la salle est occupée !")
                frameToDestroy.destroy()

            else: 
                alert("Créneau indisponible , la salle et les participants sont occupés !")
                frameToDestroy.destroy()

    return isAvailable

def common_element(list1, list2): 
    result = False
  
    for x in list1: 
  
        for y in list2: 
    
            if x == y: 
                result = True
                return result 
    return result

def deleteEventWnd(service , calid):

    deleteEv = Toplevel()
    deleteEv.geometry('500x400')
    deleteEv.title('Supprimer un évènement')
    deleteEv.configure(bg='bisque')

    subtitleLbl = Label(deleteEv , text ="Les évènements du calendrier :" , background = "bisque", font =" Helvetica 10 bold" , foreground = "red4")
    subtitleLbl.place(x = 150 , y = 20)


    deleteIcon = PhotoImage(file = "icons/delev.png") 

    events , idsEvents = getEventList(service , calid)
    eventsVar = StringVar(value = events) 

    myframe=tk.Frame(deleteEv)
    myframe.configure(bg='bisque')
    myframe.place(x= 45 , y = 70)
    scrollbar = Scrollbar(myframe) 
    scrollbar.pack(side=RIGHT, fill=Y)

    eventsToCome_box = Listbox(myframe , listvariable=eventsVar, yscrollcommand=scrollbar.set, height = 15, 
                  width = 55,  
                  bg = "wheat2",
                  borderwidth=0,
                  highlightthickness=0,
                  activestyle = 'underline',   
                  font = "Helvetica 10 bold", 
                  selectbackground = "sandybrown",
                  fg = "tan4")
    eventsToCome_box.pack(side=LEFT)
    scrollbar.config(command=eventsToCome_box.yview)
     
    deleteLbl = Label(deleteEv , text = "" , image = deleteIcon , compound ="left" , background = "bisque" )
    deleteLbl.photo = deleteIcon
    deleteLbl.place(x = 230 , y = 340)

    deleteLbl.bind("<Button-1>",lambda x:deleteEventAction(eventsToCome_box , service , calid , idsEvents))  

    #deleteicons.place(x = 380 , y = 70)
    #scrollbar.config(command=deleteicons.yview)

    #service.events().delete(calendarId='primary', eventId='eventId').execute()

def deleteEventAction(lb , service , calid , ids):
    qw=Toplevel()
    frame1 = tk.Frame(qw, background = "gray30",highlightbackground="green", highlightcolor="green",highlightthickness=5, bd=0)
    frame1.pack()
    qw.overrideredirect(1)
    qw.geometry("285x70+650+400")
    lbl = tk.Label(frame1, text="Etes-vous sur de vouloir supprimer cet évènement?" , background="azure2" , foreground = "black")
    lbl.pack()
    yes_btn = tk.Button(frame1, text="Yes", bg="light blue", fg="green",command=lambda:onSelectEventToDelete(lb , service , calid , ids ,qw), width=10)
    yes_btn.pack(padx=25, pady=10 , side=LEFT)
    no_btn = tk.Button(frame1, text="No", bg="light blue", fg="red",command=qw.destroy, width=10)
    no_btn.pack(padx=25, pady=10, side=LEFT)

def onSelectEventToDelete(lb , service , calid , ids , qw):

    gifdelwnd = Toplevel()
    gifdelwnd.geometry("300x300")


    if(lb.curselection()):
        #selectedItem = lb.get(0, END)[selected]
        selectedIdx = lb.curselection()[0]
        lb.delete(selectedIdx)
        
        #print("done!")
        lbl = GifDisplayer(gifdelwnd)
        lbl.load('animations/delev.gif') 
        lbl.pack()

        currId = ids[selectedIdx]
        service.events().delete(calendarId=calid, eventId=currId).execute()
        gifdelwnd.after(1500 , lambda:gifdelwnd.destroy())
        qw.destroy()
    #if(lb.curselection()):
    #    selected = lb.curselection()[0]
    #    print(selected)
    #    print(lbe.get(0, END)[selected])
    #else : 
    #    print("please select ! ")

def calendarWebView(service , calid , timeZone,lb):

    if(lb.curselection()):
    
        strURL = ''
        calidEncoded = urllib.parse.quote(calid)
        timezoneEncoded = urllib.parse.quote(timeZone , safe='')
        strURL = 'https://calendar.google.com/calendar/embed?src='+calidEncoded+'&ctz='+ timezoneEncoded
        webbrowser.open(strURL, new=0, autoraise=True)
    else : 
        alert("Veuilez selectionner un calendrier !")
            
    
    
def ressourcesMenuWnd(service , calid):
    if(calid):

        ressourcesWnd =Toplevel()
        ressourcesWnd.title("Ressources et informations ")
        ressourcesWnd.geometry("500x400")
        ressourcesWnd.configure(bg='bisque')

        lieuxIm = PhotoImage(file="icons/lieux.png")
        participantsIm = PhotoImage(file="icons/participants.png")


        lieuOption = Label(ressourcesWnd, image = lieuxIm ,text = " Lieux " , foreground = 'LightSalmon3',  compound = "top" , background = 'bisque' , font = "Helvetica 8 italic" )
        #addc.config(image = imgAddcalendar)
        lieuOption.photo = lieuxIm
        lieuOption.place(x=160 , y = 160)

        lieuOption.bind("<Button-1>",lambda x:lieuView(service , calid))

        participantsOption = Label(ressourcesWnd, image = participantsIm  ,text = " Participants " , foreground = 'LightSalmon3',  compound = "top" , background = 'bisque' , font = "Helvetica 8 italic")
        #addc.config(image = imgAddcalendar)
        participantsOption.photo = participantsIm
        participantsOption.place(x=290 , y = 160)

        participantsOption.bind("<Button-1>",lambda x:participantView(service , calid))
    else : 
        alert("Veuillez selectionner un calendrier !")

def lieuView(service , calid):
    lieuWnd =Toplevel()
    lieuWnd.title("Lieux")
    lieuWnd.geometry("500x400")
    lieuWnd.configure(bg='bisque')

    lieux , whatever = getAndSaveLocationAndParticipants(service , calid)
    lieuxVar = StringVar(value = lieux)

    subtitleLbl = Label(lieuWnd , text ="La liste des lieux des évènements du calendrier :" , background = "bisque", font =" Helvetica 10 bold" , foreground = "red4")
    subtitleLbl.place(x = 90 , y = 20)

    myframe=tk.Frame(lieuWnd)
    myframe.configure(bg='bisque')
    myframe.place(x= 45 , y = 70)
    scrollbar = Scrollbar(myframe) 
    scrollbar.pack(side=RIGHT, fill=Y)

    lieux_box = Listbox(myframe , listvariable=lieuxVar, yscrollcommand=scrollbar.set, height = 15, 
                  width = 55,  
                  bg = "wheat2",
                  borderwidth=0,
                  highlightthickness=0,
                  activestyle = 'underline',   
                  font = "Helvetica 10 bold", 
                  selectbackground = "sandybrown",
                  fg = "tan4")
    lieux_box.pack(side=LEFT)
    scrollbar.config(command=lieux_box.yview)


def participantView(service , calid):
    participantWnd =Toplevel()
    participantWnd.title("Lieux")
    participantWnd.geometry("500x400")
    participantWnd.configure(bg='bisque')

    subtitleLbl = Label(participantWnd , text ="La liste des participants aux différents évènements de ce calendrier : " , background = "bisque", font =" Helvetica 10 bold" , foreground = "red4")
    subtitleLbl.place(x = 30 , y = 20)

    whatever , participants = getAndSaveLocationAndParticipants(service , calid)
    participantVar = StringVar(value = participants) 

    myframe=tk.Frame(participantWnd)
    myframe.configure(bg='bisque')
    myframe.place(x= 45 , y = 70)
    scrollbar = Scrollbar(myframe) 
    scrollbar.pack(side=RIGHT, fill=Y)

    participants_box = Listbox(myframe , listvariable=participantVar, yscrollcommand=scrollbar.set, height = 15, 
                  width = 55,  
                  bg = "wheat2",
                  borderwidth=0,
                  highlightthickness=0,
                  activestyle = 'underline',   
                  font = "Helvetica 10 bold", 
                  selectbackground = "sandybrown",
                  fg = "tan4")
    participants_box.pack(side=LEFT)
    scrollbar.config(command=participants_box.yview)




def accessWindow(service , accountOwner):
    global calid
    global currentTcz
    accesswindow =Toplevel()
    accesswindow.title("Profile utilisateur ")
    accesswindow.geometry("500x400")
    accesswindow.configure(bg='bisque')
    imgProfile = PhotoImage(file="icons/profile.png")
    imgAddcalendar = PhotoImage(file="icons/addCalendar.png")
    imgDelcalendar = PhotoImage(file="icons/deleteCal.png")
    imgViewcalendar = PhotoImage(file="icons/view.png")
    imgEditcalendar = PhotoImage(file="icons/editcalendar.png")
    imgLoad = PhotoImage(file = "icons/load.png")
    imgDnld = PhotoImage(file = "icons/download.png")
    ressourcesImage = PhotoImage(file = "icons/ressources.png")

    label = Label(accesswindow, text = "Les calendriers associés à ce compte : " , width = 55, background="bisque", relief= "solid" , borderwidth = 3 , foreground = "salmon4" , font = 'Helvetica 8 bold').place(x=30,y=90)
    listcalendars , listIds , listTcz = getCalendarsNamesList(service)
    
    var1 = StringVar(value=listcalendars)
    calendars_box = Listbox(accesswindow, listvariable=var1,  height = 5, 
                  width = 49,  
                  bg = "wheat2",
                  borderwidth=0,
                  highlightthickness=0,
                  activestyle = 'underline',   
                  font = "Helvetica 10 bold", 
                  selectbackground = "sandybrown",
                  fg = "tan4") 
    
    calendars_box.place( x = 30 , y = 120)
    calendars_box.bind("<<ListboxSelect>>", lambda x:getSelectedIdCalendar(calendars_box, listIds , listcalendars , service))

    panel = Label(accesswindow, image = imgProfile , text = accountOwner.upper() , compound = "left" , width = 50 , foreground = 'white' , font = "Helvetica 12 bold")
    panel.config(background="salmon3")
    panel.photo = imgProfile 
    panel.place(x=30 , y = 20)

    editc = Button(accesswindow, image = imgEditcalendar ,text = "Gestion des évènements" ,compound = "left" ,command = lambda:eventsMenu(service , calid,calendars_box))
    editc.photo = imgEditcalendar
    editc.place(x = 30 , y = 345)

    

    viewc = Button(accesswindow, image = imgViewcalendar ,text = "Visualiser le calendrier" , width = 22 ,compound = "left" ,command = lambda:calendarWebView(service , calid , currentTcz,calendars_box))
    viewc.photo = imgViewcalendar
    viewc.place(x =200, y = 345)

    addc = Button(accesswindow, image = imgAddcalendar ,text = "Nouveau calendrier  " , compound = "left",width = 21 ,command = lambda:newcalendarWindow(service))
    #addc.config(image = imgAddcalendar)
    addc.photo = imgAddcalendar
    addc.place(x=30 , y = 300)


    deletec = Button(accesswindow, image = imgDelcalendar ,text = "Supprimer un calendrier  " , compound = "left" ,command = lambda:deleteCalendar(service , calid , accesswindow,calendars_box))
    #addc.config(image = imgAddcalendar)
    deletec.photo = imgDelcalendar
    deletec.place(x = 200 , y = 300)

    ressourcesBtn = Button(accesswindow , image = ressourcesImage , text = "\nInformations \net ressources\n"  , compound = "left" , command = lambda:ressourcesMenuWnd(service ,calid))
    ressourcesBtn.photo= ressourcesImage
    ressourcesBtn.place(x = 375, y = 300)

    loadc = Label(accesswindow, image = imgLoad ,text = "Export ICS" , foreground = 'LightSalmon3',  compound = "bottom" , background = 'bisque' , font = "Helvetica 10 italic")
    #addc.config(image = imgAddcalendar)
    loadc.photo = imgLoad
    loadc.place(x = 390 , y = 100)
    
    
    loadc.bind("<Button-1>",lambda x:insert_events(accesswindow , service , calid))  
    
    downloadc = Label(accesswindow, image = imgDnld ,text = "Import ICS" , foreground = 'LightSalmon3',compound = "bottom" , background = 'bisque', font = "Helvetica 10 italic")
    #addc.config(image = imgAddcalendar)
    downloadc.photo = imgDnld
    downloadc.place(x=390 , y = 200)  

    downloadc.bind("<Button-1>",lambda x:import_events_ics(service , calid , accountOwner , accessWindow))

def on_select(lb):
    #print(lb.get(lb.curselection()[0]))

    creds = None
    if(lb.curselection()):    
        if os.path.exists('savedClients/'+lb.get(lb.curselection()[0])):
            with open('savedClients/'+lb.get(lb.curselection()[0]), 'rb') as token:
                creds = pickle.load(token)
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
                #print("THESE ARE CREDS", creds)
        service = build('calendar', 'v3', credentials=creds)
        #insert_events(service)
        #accessWindow()
        accessWindow(service , lb.get(lb.curselection()[0]))
        
    else:
        alert("Accès impossible ! , Veuillez séléctionner un compte Google")
         

def parse_ics_files(ics):
    events = []
    with open(ics, 'r') as rf:
        ical = Calendar().from_ical(rf.read())
        ical_config = dict(ical.sorted_items())
        for i, comp in enumerate(ical.walk()):
            if comp.name == 'VEVENT':
                event = {}
                for name, prop in comp.property_items():

                    if name in ['SUMMARY', 'LOCATION']:
                        event[name.lower()] = prop.to_ical().decode('utf-8')

                    elif name == 'DTSTART':
                        event['start'] = {
                            'dateTime': prop.dt.isoformat(),
                            'timeZone': str(prop.dt.tzinfo)
                        }

                    elif name == 'DTEND':
                        event['end'] = {
                            'dateTime': prop.dt.isoformat(),
                            'timeZone': str(prop.dt.tzinfo)
                        }

                    elif name == 'SEQUENCE':
                        event[name.lower()] = prop

                    elif name == 'TRANSP':
                        event['transparency'] = prop.lower()

                    elif name == 'CLASS':
                        event['visibility'] = prop.lower()

                    elif name == 'ORGANIZER':
                        event['organizer'] = {
                            'displayName': prop.params.get('CN') or '',
                            'email': re.match('mailto:(.*)', prop).group(1) or ''
                        }

                    elif name == 'DESCRIPTION':
                        desc = prop.to_ical().decode('utf-8')
                        desc = desc.replace(u'\xa0', u' ')
                        if name.lower() in event:
                            event[name.lower()] = desc + '\r\n' + event[name.lower()]
                        else:
                            event[name.lower()] = desc

                    elif name == 'X-ALT-DESC' and 'description' not in event:
                        soup = BeautifulSoup(prop, 'lxml')
                        desc = soup.body.text.replace(u'\xa0', u' ')
                        if 'description' in event:
                            event['description'] += '\r\n' + desc
                        else:
                            event['description'] = desc

                    elif name == 'ATTENDEE':
                        if 'attendees' not in event:
                            event['attendees'] = []
                        RSVP = prop.params.get('RSVP') or ''
                        RSVP = 'RSVP={}'.format('TRUE:{}'.format(prop) if RSVP == 'TRUE' else RSVP)
                        ROLE = prop.params.get('ROLE') or ''
                        event['attendees'].append({
                            'displayName': prop.params.get('CN') or '',
                            'email': re.match('mailto:(.*)', prop).group(1) or '',
                            'comment': ROLE
                        })

                    elif name == 'ACTION':
                        event['reminders'] = {'useDefault': True}

                    else:
                        pass

                events.append(event)

    return events

def getEventObjectList(service , calid):
    page_token = None
    events = service.events().list(calendarId=calid, pageToken=page_token).execute().get('items' ,[])
    return events

def getEventList(service , calid):
    page_token = None
    listeventsRenderingNames = []
    listIdsEvents = []
    

    events = service.events().list(calendarId=calid, pageToken=page_token).execute()
    for event in events.get('items' ,[]):

        #print(event['summary'])
        if('summary' in event and 'id' in event): #testing
            listeventsRenderingNames.append("\U0001F4C5"+" "+event['summary'])
            listIdsEvents.append(event['id'])          

    return listeventsRenderingNames , listIdsEvents 

def saveLocationAndParticipantsFiles(service , calid , nameselectedCal): 

    listLocations , finalParticipantsList = getAndSaveLocationAndParticipants(service , calid)

    if not os.path.exists(('ressources/' + nameselectedCal)):
        os.mkdir('ressources/' + nameselectedCal)
    #only create the file if the user is the same  

    lieuxFile = open("ressources/"+nameselectedCal+"/lieux.txt","w")
    participantsFile =  open("ressources/"+nameselectedCal+"/participants.txt","w")

    for lieu in listLocations:
        lieuxFile.write(lieu+'\n')

    for lparticipant in finalParticipantsList:
        participantsFile.write(lparticipant+'\n')


def getAndSaveLocationAndParticipants(service , calid):

    page_token=None
    listLocations = []
    listParticipants = []
    finalParticipantsList = []

    #loading events and extract locations and participants !!!
    events = service.events().list(calendarId=calid, pageToken=page_token).execute()
    for event in events.get('items' ,[]):
        if('location' in event):
            listLocations.append(event['location'])
        if('description' in event):
            listParticipants.append(event['description'].split('\\n'))
    
    listLocations = list(dict.fromkeys(listLocations))


    #convert listParticipants from a list of lists to a list and merge all elements for rendering purposes in listbox later on !!!!!
    for sublist in listParticipants :
        for participant in sublist :
            finalParticipantsList.append(participant)
    finalParticipantsList = list(dict.fromkeys(finalParticipantsList))



    return listLocations , finalParticipantsList 

def create_ics_file(events, owner):
    global calid
    cal = Calendar()
    datediff = None
    for eventstructure in events:
        event = Event()

        #start can have two type of dates 'dateTime' or 'date'
        if('date' in eventstructure['start']):

            datediff = eventstructure['start']['date']
        elif ('dateTime' in eventstructure['start']):
            
            datediff = eventstructure['start']['dateTime']

        if('Z' in datediff): #two different formats with Z suffix or not, two different ways to parse

            yearst = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").year
            monthst = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").month
            dayst = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").day
            hourst = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").hour
            minutest = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").minute
            secondst = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").second

            yearend = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").year
            monthend = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").month
            dayend = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").day
            hourend = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").hour
            minuteend = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").minute
            secondend = dt.datetime.strptime(datediff, "%Y-%m-%dT%H:%M:%SZ").second

        else:
            yearst = dt.datetime.fromisoformat(datediff).year
            monthst = dt.datetime.fromisoformat(datediff).month
            dayst = dt.datetime.fromisoformat(datediff).day
            hourst = dt.datetime.fromisoformat(datediff).hour
            minutest = dt.datetime.fromisoformat(datediff).minute
            secondst = dt.datetime.fromisoformat(datediff).second

            yearend = dt.datetime.fromisoformat(datediff).year
            monthend = dt.datetime.fromisoformat(datediff).month
            dayend = dt.datetime.fromisoformat(datediff).day
            hourend = dt.datetime.fromisoformat(datediff).hour
            minuteend = dt.datetime.fromisoformat(datediff).minute
            secondend = dt.datetime.fromisoformat(datediff).second

        #print('type' ,type(eventstructure))
        event.add('summary', eventstructure['summary'])   
        if(len(eventstructure['start']) == 1) : 

            #print("TIMEZONE" ,eventstructure['start']['timeZone'])
            event.add('dtstart', datetime(yearst, monthst,dayst, hourst, minutest, secondst))
            
        elif (len(eventstructure['start']) == 2) : 
            event.add('dtstart', datetime(yearst, monthst,dayst, hourst, minutest, secondst,
                                     tzinfo=pytz.timezone(eventstructure['start']['timeZone'])))

        if(len(eventstructure['end']) == 1) : 

            #print("TIMEZONE" ,eventstructure['start']['timeZone'])
            event.add('dtend', datetime(yearend, monthend,dayend, hourend, minuteend, secondend))
            
        elif (len(eventstructure['end']) == 2) : 
            event.add('dtend', datetime(yearend, monthend,dayend, hourend, minuteend, secondend,
                                     tzinfo=pytz.timezone(eventstructure['end']['timeZone'])))
        if('location' in eventstructure):
            event.add('location',eventstructure['location'])
        if('description' in eventstructure):
            event.add('description',eventstructure['description'])

        cal.add_component(event)
    
    #Creating directory with accountOwner's name 
    #Creating file with calendar's name inside the directory
    #try:
    if not os.path.exists(('download_ics/' + owner.replace(" ", "_"))):
        os.mkdir('download_ics/' + owner.replace(" ", "_"))
    #only create the file if the user is the same 
    with open('download_ics/'+ owner.replace(" ", "_") +'/' + calid +'.ics', 'wb') as f:
        f.write(cal.to_ical()) 
    #except OSError:
    #    print ('Problem generating ics file')
    #else:
    #    print ("ics generated successfully")   
    
    

def import_events_ics(service , calid , profile_info , frame):
    #print("calid" , profile_info)
    page_token= None
    
    try:
        while True:
            events = service.events().list(calendarId=calid, pageToken=page_token).execute()
            create_ics_file(events.get('items', []) , profile_info)
            if not page_token:
                break
        successWithoutdestroying("Le fichier ICS est crée avec succès !\nSous le répértoire 'download_ics'")
    except:
        alert("Un probléme est survenu, impossible de créer le fichier ICS")

def insert_events(frame , service , calid):
    links = []
    imgCheck = PhotoImage(file="icons/check.png")
    
    frame.filename =  filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("ics files","*.ics"),("all files","*.*")))

    events = parse_ics_files(frame.filename)
    cpt = 0 
    for i, event in enumerate(events):
        print(event)
        event = service.events().insert(calendarId=calid,sendNotifications=True, body=event).execute()
        #print ('Event created: %s' % (event.get('htmlLink')))
        links.append(event.get('htmlLink'))
        cpt +=1
    #runProgressSave(frame , "Les évènements du fichier "+frame.filename+" ont bien été exporté dans le calendrier séléctionné ! \n Veuillez cliquez sur le lien ci-dessus afin de visualiser le calendrier  " + event.get('htmlLink'))
        #print ('Event created: %s' % (event.get('htmlLink'))
    eventsInsertionWnd(frame , " Le fichier a bien été exporté! \n        " + str(cpt) + "évènements créés " , links , frame.filename , cpt)
    print(cpt)

def import_events(service):
    now = datetime.datetime.utcnow().isoformat() + 'Z' 
    events_result = service.events().list(calendarId='primary', timeMin=now,
                                            maxResults=10, singleEvents=True, 
                                           orderBy='startTime').execute()
    getCalendarsNamesList(service)

    #events = events_result.get('items', [])
    #if(len(events) == 0):
    #    alert("Pas d'evenements dans le calendrier")
        
    #else :
    #    for event in events:
            #start = event['start'].get('dateTime', event['start'].get('date'))
            #print(start,event['summary'])
    #        print("----IMPORTED----")
    #        imported_event = service.events().import_(calendarId='primary', body=event).execute()
    #        print (imported_event)



def main():
    root = tk.Tk() 
    root.title("Gestion Google Calendar ")
    root.geometry("500x400")
    root.configure(bg='bisque')

    client_box = Listbox(root)

    listc , creationdates  = loadClientstoAlist()
    
    #print(*listc, sep = "\n") 

    if(len(listc)==0):

        noclientlbl = tk.Label(root, text ='Aucun compte Google Calendar enregistré' , font = "Helvetica 8 italic" , bg ='bisque' , fg = 'red')
        noclientlbl.place(x =  120 , y =170)

    else:   
        varclients = StringVar(value=listc)
        vardates = StringVar(value = creationdates)
        client_box.configure(listvariable=varclients ,  height = 10,  
                      width = 45,  
                      bg = "wheat2",
                      borderwidth=0,
                      highlightthickness=0,
                      activestyle = 'underline',   
                      font = "Helvetica 11 bold", 
                      selectbackground = "sandybrown",
                      fg = "tan4")  

        #client_box.pack(side=LEFT, padx=40, pady=40)
        client_box.place( x = 60 , y = 110)

        creationdates_box = Listbox(root, listvariable=vardates ,  height = 11,  
                      width = 20,  
                      bg = "wheat2",
                      borderwidth=0,
                      highlightthickness=0,
                      activestyle = 'underline',   
                      font = "Helvetica 10 italic", 
                      selectbackground = "sandybrown",
                      fg = "tan4") 
        creationdates_box.place(x = 260 , y = 110)
        imgAccess = PhotoImage(file="icons/enter.png")
        loadUserOnClick = Button(root , text="Acceder au compte Google    " , image = imgAccess , compound="left" ,command = lambda:on_select(client_box)) 
        loadUserOnClick.place( x = 255 , y = 320)
    
    
    #TODO BIND WITH DOUBLE CLICK 
    #client_box.bind('<<ListboxSelect>>', lambda x:on_select(client_box))
    
    urlAuth = getURL()
    imgAdd = PhotoImage(file="icons/add.png")
    addGaccount = Button(root , text="Ajouter un compte  " , image = imgAdd ,compound="left" ,command = lambda:link_window(urlAuth , client_box))
    #addGaccount.config(image=imgAdd)
    #addGaccount.pack()
    addGaccount.place( x = 150 , y = 20)

    #loadTokens()
    #service = build('calendar', 'v3', credentials=creds)
    
    
    #btn = Button(root, text ='Choose credentials', command = lambda:open_file(root)) 
    #btn.place( x = 150 , y = 200) 

    #btnAccess = Button(root, text ='Access Calendar API ', command = lambda:giveLinkAndGetProfileInfos()) 
    #btnAccess.place( x = 150 , y = 180) 


    root.mainloop()

main()

